﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;
using System.Threading;
using System.Reflection;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Text;

namespace IndigmaAutomaticDisconnect
{
    class Program
    {
        #region globals and consts
        const int _data = 1;
        const int _title = 0;
        const int SW_HIDE = 0;
        const int SW_SHOW = 1;
        
        private const uint WM_GETTEXT = 0x000D;
        #endregion

        #region dll imports

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn,
            IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam,
            StringBuilder lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);


        #endregion

        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="processId">the id of the process</param>
        /// <returns>the process as an IEnumerable<IntPtr></returns>
        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);

            return handles;
        }

        /// <summary>
        /// checks if the process given is running
        /// </summary>
        /// <param name="mainProcessName">the name of the main process (the program) </param>
        /// <param name="windowName">the name of the window runing</param>
        /// <returns> true if the process is running and false otherwise</returns>
        public static bool IsProcessRunning(string mainProcessName, string windowName)
        {
            IEnumerable<int> ids = Process.GetProcessesByName(mainProcessName).Select(proc => proc.Id).Distinct();
            IEnumerable<IntPtr> processWindowHandles;
            StringBuilder message = new StringBuilder(1000);
            string lowerWindowName = windowName.Trim().ToLower();

            foreach (int id in ids)
            {
                try
                {
                    processWindowHandles = EnumerateProcessWindowHandles(id);
                    foreach (IntPtr handle in processWindowHandles)
                    {
                        _ = SendMessage(handle, WM_GETTEXT, message.Capacity, message);
                        if (message.ToString().Trim().ToLower() == lowerWindowName)
                        {
                            return true;
                        }
                    }
                }
                catch { }
            }
            return false;
        }


        /// <summary>
        /// searches for a process with the given name
        /// </summary>
        /// <param name="mainWindowTitle">the name of the process</param>
        /// <returns>Return The Correct Process Or Null If not exists</returns>
        public static Process GetRunningProcess(string mainWindowTitle)
        {
            Process[] processlist = Process.GetProcesses();

            var processToReturn = processlist
                .FirstOrDefault(process => (!String.IsNullOrEmpty(process.MainWindowTitle) && process.MainWindowTitle == mainWindowTitle));
            processlist = null;

            return processToReturn;
        }

        /// <summary>
        /// checks if the program is already running
        /// </summary>
        /// <returns>if the program is running</returns>
        static bool IsProgramAlreadyRunning()
        {
            return Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1;
        }

        /// <summary>
        /// makes the program start whenever the operating system starts
        /// </summary>
        /// <param name="runOnStartUp">to run the program at startup or not</param>
        static void RunAtStartUp(bool runOnStartUp)
        {
            RegistryKey reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", runOnStartUp);
            string programName = "IndigmaAddon";
            if (reg.GetValue(programName) == null)
            {
                reg.SetValue(programName, Application.ExecutablePath.ToString());
            }
            reg.Close();
        }

        /// <summary>
        /// makes the window given the foreground window
        /// </summary>
        /// <param name="handle">the window handle</param>
        /// <param name="viewState">the view state</param>
        public static void MakeWindowForeground(IntPtr handle, int viewState)
        {
            _ = ShowWindow(handle, viewState);
            _ = SetForegroundWindow(handle);
        }

        static void Main(string[] args)
        {
            if (!IsProgramAlreadyRunning()) //if this is the only instance of the program
            {
                RunAtStartUp(true);
                #region parameters
                bool isRunning = true;
                DateTime startOfProgram = DateTime.Now;
                IntPtr consoleHandle = GetConsoleWindow();
                string[] lineRead;
                bool isTesting;
                int timeOutInMinutes, messageBoxTimeoutInSecs;
                string processTitle, textboxMessage, closingKey, windowName;
                Process messageboxProcess;
                bool last_check_status = false;
                #endregion


                // Hide the console
                ShowWindow(consoleHandle, SW_HIDE);

                using(FileStream readStream = new FileStream(Directory.GetCurrentDirectory() + "\\config.txt", FileMode.Open, FileAccess.Read))
                using (StreamReader reader = new StreamReader(readStream))
                {
                    //read the main process name
                    lineRead = (reader.ReadLine()).Split(':');
                    processTitle = lineRead[_data].Trim();

                    //read the window name
                    lineRead = (reader.ReadLine()).Split(':');
                    windowName = lineRead[_data].Trim();

                    //read the time untill closing the window (minutes)
                    lineRead = (reader.ReadLine()).Split(':');
                    timeOutInMinutes = int.Parse(lineRead[_data]);

                    //read the time from displaying the message until closing (seconds)
                    lineRead = (reader.ReadLine()).Split(':');
                    messageBoxTimeoutInSecs = int.Parse(lineRead[_data]);

                    //read the textbox message
                    lineRead = (reader.ReadLine()).Split(':');
                    textboxMessage = lineRead[_data];

                    //read the key to send 
                    lineRead = (reader.ReadLine()).Split(':');
                    closingKey = lineRead[_data].Trim();

                    isTesting = (timeOutInMinutes == -1);

                    lineRead = null;
                }//using reader

                GC.Collect();
                TimeSpan timeBetweenStartToClosing = TimeSpan.FromMinutes(timeOutInMinutes);

                string message = string.Format(textboxMessage, messageBoxTimeoutInSecs);
                string caption = "closing the system";
                MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                DialogResult result = DialogResult.OK;

                while (isRunning)
                {
                    if (!isTesting)
                    {
                        if (IsProcessRunning(processTitle, windowName) && IsWindowVisible(GetRunningProcess(windowName).Handle))
                        {
                            if (!last_check_status)
                            {
                                startOfProgram = DateTime.Now;
                                last_check_status = true;
                            }
                            else //if the program is in RF Testing
                            {
                                if (DateTime.Now.Subtract(startOfProgram) >= timeBetweenStartToClosing)
                                {

                                    Thread thread = new Thread(() =>
                                    {
                                        result = MessageBox.Show(message, caption, buttons);
                                    });
                                    thread.Start();
                                    while (GetRunningProcess(caption) == null) // wait until the messagebox appears
                                    {
                                        Thread.Sleep(250);
                                    }

                                    messageboxProcess = GetRunningProcess(caption);

                                    MakeWindowForeground(messageboxProcess.MainWindowHandle, SW_SHOW);
                                    
                                    Thread.Sleep(1000); //sleep for a quarter of a second

                                    if (!thread.Join(TimeSpan.FromSeconds(messageBoxTimeoutInSecs)))
                                    {
                                        MakeWindowForeground(messageboxProcess.MainWindowHandle, SW_SHOW);
                                        
                                        SendKeys.SendWait("{ENTER}");
                                        result = DialogResult.OK;

                                        messageboxProcess = null;
                                    }

                                    if (result == DialogResult.Cancel)
                                    {
                                        startOfProgram = DateTime.Now;
                                    }
                                    else // to close
                                    {
                                        while (IsProcessRunning(processTitle, windowName) && IsWindowVisible(GetRunningProcess(windowName).Handle))
                                        {
                                            SendKeys.SendWait("{" + closingKey + "}"); //the key to close the program
                                            Thread.Sleep(100);
                                        }
                                    }
                                }
                                GC.Collect();
                            }
                        }
                        else // If RF Testing not running
                        {
                            last_check_status = false;
                        }
                    }
                    
                    Thread.Sleep(1000);
                }//while
            }//if not already running
        }//main


    }//class
}//namespace
