# Automatic Disconnect

This project sends a key of your choosing if a process given is running for an ammount of time set in the config file. In addition the project creates pa log file and a CSV file that smmerizes the system given commands.

## Getting Started

In order to start, you have to first compile the program. After youv'e done that you supposed to have with the .exe file a file named 
```

config.txt

```
this file is your configuration file. You should fill in the config file as stated in order to config the project correctly.

```
<Config>
	<ProcessName></ProcessName> (the name of the process to check)
	<WindowName></WindowName>(the name of the window to check)
	<LogPath></LogPath> (the path to the process given log )
	<MessageBoxTimeoutInSecs></MessageBoxTimeoutInSecs> (the timeout of the warning messagebox)
	
	<!-- RF testing mode-->
	<RFTestingCaption></RFTestingCaption> (the caption for the messagebox)
	<RFTestingTextboxMessage></RFTestingTextboxMessage> (the message inside the messagebox)
	<ClosingKey></ClosingKey> (the closing key to send in the firmat of SendKey.SendWait)
	<RFTestingStartingLine></RFTestingStartingLine> (the line in the process log that states the start of the mode)
	<RFTestingClosingLine></RFTestingClosingLine> (the line in the process log that states the end of the mode)
	<TimeOutInMinutes></TimeOutInMinutes> (the time to wait until alert in minutes)
	
	<!-- Download mode-->
	<DownloadCaption></DownloadCaption> (the caption for the messagebox)
	<TextboxDownloadMessage></TextboxDownloadMessage> (the message inside the messagebox)
	<TimeUntilCheckDownload></TimeUntilCheckDownload> (the time to wait until alert in minutes)
	<DownloadStartingLine>Start Download</DownloadStartingLine> (the line in the process log that states the start of the mode)
	<DownloadClosingLine>End Download</DownloadClosingLine> (the line in the process log that states the end of the mode)
	
	<!-- Record mode-->
	<RecordCaption></RecordCaption> (the caption for the messagebox)
	<TextboxRecordMessage></TextboxRecordMessage> (the message inside the messagebox)
	<TimeUntilCheckRecord></TimeUntilCheckRecord>(the time to wait until alert in minutes)
	<RecordStartingLine></RecordStartingLine> (the line in the process log that states the start of the mode)
	<RecordClosingLine></RecordClosingLine> (the line in the process log that states the end of the mode)
	
	<!-- Control mode -->
	<ControlStartingLine></ControlStartingLine> (the line in the process log that states the start of the mode)
	<ControlClosingLine></ControlClosingLine> (the line in the process log that states the end of the mode)
	
</Config>

```

This is the file that the program takes its parameters from (thats why the order is important).