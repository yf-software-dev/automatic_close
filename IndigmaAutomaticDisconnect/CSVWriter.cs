﻿using IndigmaAutomaticDisconnect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IndigmaAutomaticDisconnect
{
    public class CSVWriter
    {
        public string PathToFile { get; set; }
        public CSVWriter(string path)
        {
            PathToFile = path;
        }

        public void Write(List<IMode> modeList)
        {
            StringBuilder builder = new StringBuilder();
            TimeSpan totalTime;
            foreach (IMode mode in modeList)
            {
                totalTime = TimeSpan.Zero;
                _ = builder.Append(string.Format("{0} mode " + Environment.NewLine, mode.ModeName));
                _ = builder.Append("Start time , End time , Duration " + Environment.NewLine);
                foreach (KeyValuePair<DateTime, TimeSpan> time in mode.GetTimeline())
                {
                    _ = builder.Append(string.Format("{0} , {1} , {2}" + Environment.NewLine, time.Key.TimeOfDay, time.Key.TimeOfDay + time.Value, time.Value));
                    totalTime += time.Value;
                }
                _ = builder.Append(string.Format("Total time in mode: {0} " + Environment.NewLine, totalTime.ToString()));
            }
            foreach (IMode mode in modeList)
            {
                foreach (KeyValuePair<DateTime, TimeSpan> time in mode.GetTimeline())
                {
                    mode.RemoveFromTimeline(time);
                }
            }
            using (FileStream csvStream = new FileStream(PathToFile, FileMode.OpenOrCreate, FileAccess.Write))
            using (StreamWriter writer = new StreamWriter(csvStream))
            {
                writer.Write(builder);
            }
        }
    }
}
