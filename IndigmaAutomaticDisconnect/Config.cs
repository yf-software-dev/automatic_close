﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IndigmaAutomaticDisconnect
{
    public class Config
    {
        public string ProcessName { get; set; }
        public string WindowName { get; set; } 
        public string LogPath { get; set; } 
        public int MessageBoxTimeoutInSecs { get; set; }

        #region RFTesting mode
        public string RFTestingCaption { get; set; }
        public string RFTestingTextboxMessage { get; set; }
        public string ClosingKey { get; set; }
        public double TimeOutInMinutes { get; set; }
        public string RFTestingStartingLine { get; set; }
        public string RFTestingClosingLine { get; set; }
        #endregion

        #region Download mode
        public string DownloadCaption { get; set; }
        public string TextboxDownloadMessage { get; set; }
        public double TimeUntilCheckDownload { get; set; }
        public string DownloadStartingLine { get; set; }
        public string DownloadClosingLine { get; set; }
        #endregion

        #region Record mode
        public string RecordCaption { get; set; }
        public string TextboxRecordMessage { get; set; }
        public double TimeUntilCheckRecord { get; set; }
        public string RecordStartingLine { get; set; }
        public string RecordClosingLine { get; set; }
        #endregion

        #region Control mode
        public string ControlStartingLine { get; set; }
        public string ControlClosingLine { get; set; }
        #endregion
    }
}
