﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace IndigmaAutomaticDisconnect
{
    public class ImportedFunctions
    {
        public const int SW_HIDE = 0;
        public const int SW_SHOW = 1;
        private const uint WM_GETTEXT = 0x000D;

        #region dll imports
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn,
    IntPtr lParam);
        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam,
    StringBuilder lParam);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();

        //[return: MarshalAs(UnmanagedType.Bool)]
        //[DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //public static extern bool SetForegroundWindow(IntPtr hwnd);
        #endregion

        public static bool IsWindowsVisible(List<IntPtr> handlesList)
        {
            foreach (IntPtr handle in handlesList)
            {
                if (IsWindowVisible(handle))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// returns the window name from the window handle
        /// </summary>
        /// <param name="windowHandle">the handle of the window</param>
        /// <returns>the name of the window</returns>
        public static string GetWindowName(IntPtr windowHandle)
        {
            StringBuilder message = new StringBuilder(1000);
            _ = SendMessage(windowHandle, WM_GETTEXT, message.Capacity, message);
            string windowName = message.ToString();
            return windowName;
        }


        /// <summary>
        /// change the visible window in a given window list`s view state
        /// </summary>
        /// <param name="windowHandle">the handle of the window you want to chagne the view state of</param>
        /// <param name="viewState">the view state for the window to be at</param>
        public static void ChangeWindowViewState(IntPtr windowHandle, int viewState)
        {

            if (IsWindowVisible(windowHandle))
            {
                _ = ShowWindow(windowHandle, viewState);
                if (viewState == SW_SHOW)
                {
                    _ = SetForegroundWindow(windowHandle);
                }
            }


        }

        /// <summary>
        /// change the visible window in a given window list`s view state
        /// </summary>
        /// <param name="windowHandles">the handle of the window you want to chagne the view state of</param>
        /// <param name="viewState">the view state for the window to be at</param>
        public static void ChangeWindowViewState(List<IntPtr> windowHandles, int viewState)
        {
            foreach (IntPtr handle in windowHandles)
            {
                if (IsWindowVisible(handle))
                {
                    _ = ShowWindow(handle, viewState);
                    _ = SetForegroundWindow(handle);
                }
            }

        }

        /// <summary>
        /// returns an IEnumerable(IntPtr) of the handles of all the windows of the process
        /// </summary>
        /// <param name="processId">the id of the process</param>
        /// <returns>the process as an IEnumerable<IntPtr></returns>
        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            List<IntPtr> handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);

            return handles;
        }

        /// <summary>
        /// returns the window handle from a given process
        /// </summary>
        /// <param name="processId">the id of the process to check</param>
        /// <param name="windowName">the name of the window to search for</param>
        /// <returns>the handle of the relevent window</returns>
        public static List<IntPtr> GetWindowHandle(int processId, string windowName)
        {
            IEnumerable<IntPtr> processWindowHandles = EnumerateProcessWindowHandles(processId);
            List<IntPtr> relevantWindowHandles = new List<IntPtr>();
            try
            {
                foreach (IntPtr handle in processWindowHandles)
                {
                    string currentWindowName = GetWindowName(handle);
                    if (!string.IsNullOrEmpty(currentWindowName) && currentWindowName == windowName)
                    {
                        relevantWindowHandles.Add(handle);
                    }

                }
                if (relevantWindowHandles.Count == 0)
                {
                    Console.WriteLine("Window Name Not Correct");
                }
                return relevantWindowHandles;
            }
            catch { }
            return relevantWindowHandles;
        }

        /// <summary>
        /// searches for a process with the given name
        /// </summary>
        /// <param name="processName">the name of the process</param>
        /// <returns>Return The Correct Process or Null If does not exist</returns>
        public static Process GetRunningProcess(string processName)
        {
            if (!String.IsNullOrEmpty(processName))
            {
                return Process.GetProcessesByName(processName).FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// returns the last line ofa given log
        /// </summary>
        /// <param name="logReader">the reader of the log</param>
        /// <returns>the last line of the given log</returns>
        public static string GetLastLineOfFile(StreamReader logReader)
        {
            string filedata, lastline = "";
            if (logReader != null)
            {
                // Assuming a line doesnt have more than 500 characters
                long offset = Math.Max(logReader.BaseStream.Length - 500, 0);

                //directly move the last 500th position
                logReader.BaseStream.Seek(offset, SeekOrigin.Begin);

                //From there read lines, not whole file
                while (!logReader.EndOfStream)
                {
                    filedata = logReader.ReadLine();

                    // Iterate to see last line
                    if (logReader.Peek() == -1)
                    {
                        lastline = filedata;
                    }
                }
            }
            return lastline;
        }

        /// <summary>
        /// continue reading from the file where youv`e stopped
        /// </summary>
        /// <param name="logReader">a reader to the file</param>
        /// <returns>the next line after youve stopped reading</returns>
        public static string GetLastLineReadFromFile(StreamReader logReader)
        {
            string lastline = "";
            //bool temp;
            if (logReader != null)
            {
                //temp = logReader.EndOfStream;

                if (!logReader.EndOfStream)
                {
                    lastline = logReader.ReadLine();
                }
            }

            return lastline;
        }

        /// <summary>
        /// returns if the line given is the last line of a given log
        /// </summary>
        /// <param name="stringToCheck">the string to check</param>
        /// <param name="logReader">the reader of the log to check</param>
        /// <returns>if the line given is the last line of the log</returns>
        public static bool IsLineInLastLineOfLog(string stringToCheck, StreamReader logReader)
        {
            try
            {
                return GetLastLineOfFile(logReader).Contains(stringToCheck);
            }
            catch { return false; }
        }

        /// <summary>
        /// returns the latest file in the given folder
        /// </summary>
        /// <param name="LogPath">the path to the folder to check</param>
        /// <returns>FileInfo of the latest file</returns>
        public static FileInfo GetLatestFile(string LogPath)
        {
            DirectoryInfo directory = new DirectoryInfo(LogPath);
            FileInfo relevantFile = directory.GetFiles().OrderByDescending(file => file.LastWriteTime).FirstOrDefault();
            return relevantFile;
        }

        /// <summary>
        /// returns the time from a given line
        /// </summary>
        /// <param name="line">the line to convert</param>
        /// <returns>the time from the line as DateTime</returns>
        public static DateTime GetTimeFromLine(string line)
        {
            DateTime dateTime = DateTime.MinValue;
            if (line != "")
            {

                string[] lineSplited = line.Split('-');

                dateTime = DateTime.Parse(lineSplited[0]);
            }
            return dateTime;

        }

        /// <summary>
        /// writes a given message to the log
        /// </summary>
        /// <param name="message">the message to write</param>
        /// <param name="logger">the log to write to</param>
        public static void WriteToLog(string message, StreamWriter logger)
        {
            if (logger != null)
                logger.WriteLine($"{ DateTime.Now:dd/MM/yyyy - HH:mm:ss} - " + message + Environment.NewLine);
        }

        public static void WriteToLogs(string message, List<StreamWriter> logs)
        {
            foreach(StreamWriter log in logs)
            {
                WriteToLog(message, log);
            }
        }

    }//class
}//namespace
