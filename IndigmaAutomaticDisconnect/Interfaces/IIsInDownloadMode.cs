﻿namespace IndigmaAutomaticDisconnect.Interfaces
{
    public interface IIsInDownloadMode
    {
        bool IsInMode { get; }
    }
}
