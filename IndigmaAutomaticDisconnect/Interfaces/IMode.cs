﻿using System;
using System.Collections.Generic;

namespace IndigmaAutomaticDisconnect.Interfaces
{
    public interface IMode
    {
        bool CheckLine(string line);
        List<KeyValuePair<DateTime, TimeSpan>> GetTimeline();
        void RemoveFromTimeline(KeyValuePair<DateTime, TimeSpan> pair);
        string ModeName { get; }
        bool IsInMode { get; }
    }
}
