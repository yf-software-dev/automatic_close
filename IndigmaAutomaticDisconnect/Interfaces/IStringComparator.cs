﻿namespace IndigmaAutomaticDisconnect.Interfaces
{
    public interface IStringComparator
    {
        bool DoesLineMeesCondition(string line);
    }
}
