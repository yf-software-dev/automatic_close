﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace IndigmaAutomaticDisconnect
{
    public class MessageBoxController
    {
        #region parameters
        public IntPtr Windowhandle { get; set; }
        public string Caption { get; set; }
        public string Message { get; set; }
        public int TimeOut { get; set; }
        public MessageBoxButtons Buttons { get; set; }
        public DialogResult Result { get; set; }
        #endregion

        public MessageBoxController(string caption, string message, MessageBoxButtons buttons, int MessageBoxTimeoutInSecs)
        {
            Result = DialogResult.None;
            Caption = caption;
            Message = message;
            Buttons = buttons;
            TimeOut = MessageBoxTimeoutInSecs;
            Windowhandle = IntPtr.Zero;
        }


        // Event For Result (OnResultEvent)


        public void Show(StreamWriter logger = null)
        {
            Thread messageboxThread = new Thread(() =>
            {
                Result = MessageBox.Show(Message, Caption, Buttons);
            });
            messageboxThread.Start();

            Windowhandle = ImportedFunctions.GetWindowHandle(Process.GetCurrentProcess().Id, Caption).FirstOrDefault();
            while (Windowhandle == null) // wait until the messagebox appears
            {
                Thread.Sleep(250);
                Windowhandle = (ImportedFunctions.GetWindowHandle(Process.GetCurrentProcess().Id, Caption)).FirstOrDefault();
            }

            ImportedFunctions.WriteToLog("Showing closing messagebox.", logger);
            ImportedFunctions.SetForegroundWindow(Windowhandle);

            Thread.Sleep(1000);

            if (!messageboxThread.Join(TimeSpan.FromSeconds(TimeOut)))
            {
                ImportedFunctions.WriteToLog("No user input detected for " + TimeOut + " seconds. Closing messagebox.", logger);

                ImportedFunctions.ChangeWindowViewState(Windowhandle, ImportedFunctions.SW_SHOW);
                SendKeys.SendWait("{ENTER}");
                Thread.Sleep(100);
                if (messageboxThread.IsAlive)
                    messageboxThread.Abort();
                Result = DialogResult.Yes;
            }
        }
    }
}
