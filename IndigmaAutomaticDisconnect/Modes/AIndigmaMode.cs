﻿using IndigmaAutomaticDisconnect.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace IndigmaAutomaticDisconnect
{
    public abstract class AIndigmaMode : IMode
    {
        #region parameters
        protected List<KeyValuePair<DateTime, TimeSpan>> timeline;
        protected DateTime startTime;
        protected TimeSpan duration;
        public List<StreamWriter> Logger { get; set; }
        protected abstract IStringComparator startStringComparator { get; set; }
        protected abstract IStringComparator endStringComparator { get; set; }
        public bool IsInMode { get; protected set; }
        #endregion

        #region abstract functions
        public abstract string ModeName { get; }

        public AIndigmaMode()
        {
            timeline = new List<KeyValuePair<DateTime, TimeSpan>>();
            duration = TimeSpan.Zero;
            IsInMode = false;
        }

        /// <summary>
        /// this function check the log to see if the last line is the line to start the current mode
        /// </summary>
        /// <param name="line">the line to check</param>
        /// <returns>true if the system is at the start of the mode and false otherwise</returns>
        public bool IsStartMode(string line)
        {
            if (line != null)
            {
                return startStringComparator.DoesLineMeesCondition(line);
            }
            return false;
        }

        /// <summary>
        /// this function check the log to see if the last line is the line to end the current mode
        /// </summary>
        /// <param name="line">the line to check</param>
        /// <returns>true if the system is at the end of the mode and false otherwise</returns>
        public bool IsEndMode(string line)
        {
            if (line != null)
            {
                return endStringComparator.DoesLineMeesCondition(line);
            }
            return false;
        }
        #endregion

        /// <summary>
        /// this function takes a line and checks the log (using IsStartMode and IsEndMode) if the system is in the current mode
        /// </summary>
        /// <param name="line">the line to check</param>
        /// <returns>true if the system is in the start of the mode or at the end of the mode and false otherwise</returns>
        public virtual bool CheckLine(string line)
        {
            if (line == "")
            {
                return false;
            }
            DateTime currentLineDateTime = ImportedFunctions.GetTimeFromLine(line);
            if (IsStartMode(line))
            {
                ImportedFunctions.WriteToLogs("The system has entered " + ModeName + " mode.", Logger);
                startTime = currentLineDateTime;
                IsInMode = true;
                return true;
            }
            else if (IsEndMode(line))
            {
                if (IsInMode)
                {
                    if (startTime == null)
                    {
                        IsInMode = false;
                        return false;
                    }
                    ImportedFunctions.WriteToLogs("The system has exited " + ModeName + " mode.", Logger);
                    duration = currentLineDateTime - startTime;
                    timeline.Add(new KeyValuePair<DateTime, TimeSpan>(startTime, duration));
                }
                IsInMode = false;
                return true;
            }
            return false;
        }

        /// <summary>
        /// this function return the timeline of the current mode (the start DateTime and the timespan to the end)
        /// </summary>
        /// <returns>the timeline of the current mode</returns>
        public List<KeyValuePair<DateTime, TimeSpan>> GetTimeline()
        {
            return timeline;
        }

        public void RemoveFromTimeline(KeyValuePair<DateTime, TimeSpan> pair)
        {
            _ = timeline.Remove(pair);
        }
    }
}
