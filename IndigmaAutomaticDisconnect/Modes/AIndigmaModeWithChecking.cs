﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace IndigmaAutomaticDisconnect.Modes
{
    public abstract class AIndigmaModeWithChecking : AIndigmaMode
    {
        #region properties
        protected MessageBoxController MessageBox;
        protected Thread CheckTimeThread;
        #endregion
        public abstract double TimeUntilCheckInMinutes { get; }
        public abstract int MessageboxTimeoutInSecs { get; }

        public abstract bool CheckMode();


        public AIndigmaModeWithChecking(string messageboxCaption, string messageboxMessage, MessageBoxButtons messageBoxButtons)
        {
            MessageBox = new MessageBoxController(messageboxCaption, messageboxMessage, messageBoxButtons, MessageboxTimeoutInSecs);
        }
        public override bool CheckLine(string line)
        {
            if(line == "")
            {
                return false;
            }
            if (IsInMode && CheckMode())
            {
                LogInModeTooLong();
            }
            DateTime currentLineDateTime = ImportedFunctions.GetTimeFromLine(line);
            if (IsStartMode(line))
            {
                ImportedFunctions.WriteToLogs("The system has entered " + ModeName + " mode.", Logger);
                startTime = currentLineDateTime;
                IsInMode = true;
                CheckTimeThread = new Thread(() => 
                {
                    while (IsInMode)
                    {
                        if (CheckMode())
                        {
                            LogInModeTooLong();
                        }
                        Thread.Sleep(10000);//check every 10 seconds
                    }
                });
                CheckTimeThread.Start();
                return true;
            }
            else if (IsEndMode(line))
            {
                if (IsInMode)
                {
                    if (startTime == null)
                    {
                        IsInMode = false;
                        return false;
                    }
                    ImportedFunctions.WriteToLogs("The system has exited " + ModeName + " mode.", Logger);
                    duration = currentLineDateTime - startTime;
                    timeline.Add(new KeyValuePair<DateTime, TimeSpan>(startTime, duration));
                }
                IsInMode = false;
                CheckTimeThread.Abort();
                return true;
            }
            return false;
        }//CheckLine

        private void LogInModeTooLong()
        {
            ImportedFunctions.WriteToLogs("The system was in " + ModeName + " mode for " + TimeUntilCheckInMinutes + " minutes.", Logger);
        }
    }//class
}//namespace
