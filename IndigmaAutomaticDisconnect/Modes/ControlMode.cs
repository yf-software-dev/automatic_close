﻿using IndigmaAutomaticDisconnect.Interfaces;

namespace IndigmaAutomaticDisconnect.Modes
{
    class ControlMode : AIndigmaMode
    {
        public override string ModeName => "Control";


        protected override IStringComparator startStringComparator { get; set; }
        protected override IStringComparator endStringComparator { get; set; }

        public ControlMode(string startingLine, string closingLine)
        {
            startStringComparator = new FullStringComparator(startingLine);
            endStringComparator = new FullStringComparator(closingLine);
        }
    }
}
