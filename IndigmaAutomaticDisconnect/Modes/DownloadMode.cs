﻿using IndigmaAutomaticDisconnect.Interfaces;
using System;
using System.Windows.Forms;

namespace IndigmaAutomaticDisconnect.Modes
{
    public class DownloadMode : AIndigmaModeWithChecking, IIsInDownloadMode
    {
        #region properties
        public override string ModeName => "Download";

        public override double TimeUntilCheckInMinutes { get; }
        public override int MessageboxTimeoutInSecs => 15;

        protected override IStringComparator startStringComparator { get; set; }

        protected override IStringComparator endStringComparator { get; set; }
        #endregion

        public DownloadMode(string startingLine, string closingLine, double timeUntilCheck, string messageboxCaption, string messageboxMessage, MessageBoxButtons messageBoxButtons) :
           base(messageboxCaption, messageboxMessage, messageBoxButtons)
        {
            TimeUntilCheckInMinutes = timeUntilCheck;
            startStringComparator = new FullStringComparator(startingLine);
            endStringComparator = new FullStringComparator(closingLine);
        }

        public override bool CheckMode()
        {
            return (DateTime.Now - startTime) > TimeSpan.FromMinutes(TimeUntilCheckInMinutes);
        }
    }
}
