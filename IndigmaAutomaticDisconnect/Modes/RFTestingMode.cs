﻿using IndigmaAutomaticDisconnect.Interfaces;

namespace IndigmaAutomaticDisconnect.Modes
{
    public class RFTestingMode : AIndigmaMode
    {
        #region properties
        public override string ModeName => "RFTesting";
        protected override IStringComparator startStringComparator { get; set; }
        protected override IStringComparator endStringComparator { get; set; }

        #endregion
        public RFTestingMode(string startingLine, string closingLine)
        {
            startStringComparator = new FullStringComparator(startingLine);
            endStringComparator = new FullStringComparator(closingLine);
        }
    }
}
