﻿using IndigmaAutomaticDisconnect.Interfaces;
using System;
using System.Windows.Forms;

namespace IndigmaAutomaticDisconnect.Modes
{
    public class RecordMode : AIndigmaModeWithChecking
    {
        #region properties
        public override string ModeName => "Record";

        public override double TimeUntilCheckInMinutes { get; }

        public override int MessageboxTimeoutInSecs { get; }

        protected override IStringComparator startStringComparator { get; set; }
        protected override IStringComparator endStringComparator { get; set; }

        #endregion
        public RecordMode(string startingLine, string closingLine, double timeUntilCheck, string messageboxCaption, string messageboxMessage, MessageBoxButtons messageBoxButtons) :
            base(messageboxCaption, messageboxMessage, messageBoxButtons)
        {
            TimeUntilCheckInMinutes = timeUntilCheck;
            startStringComparator = new FullStringComparator(startingLine);
            endStringComparator = new FullStringComparator(closingLine);
        }

        public override bool CheckMode()
        {
            return (startTime - DateTime.Now) > TimeSpan.FromMinutes(TimeUntilCheckInMinutes);
        }
    }
}
