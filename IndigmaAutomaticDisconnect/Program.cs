﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Collections.Generic;
using System.Xml.Serialization;
using IndigmaAutomaticDisconnect.Modes;
using IndigmaAutomaticDisconnect.Interfaces;

namespace IndigmaAutomaticDisconnect
{
    class Program
    {

        #region globals and consts
        static FileStream fullfiledata;
        #endregion


        /// <summary>
        /// checks if the program is already running
        /// </summary>
        /// <returns>if the program is running</returns>
        public static bool IsProgramAlreadyRunning()
        {
            return Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1;
        }

        private static void WriteMessageboxInputToLog(DialogResult result, List<StreamWriter> loggers)
        {
            switch (result)
            {
                case DialogResult.OK:
                    {
                        ImportedFunctions.WriteToLogs("User clicked Ok on messagebox.", loggers);
                        break;
                    }
                case DialogResult.None:
                    {
                        ImportedFunctions.WriteToLogs("No messagebox was shown.", loggers);
                        break;
                    }
                case DialogResult.Yes:
                    {
                        ImportedFunctions.WriteToLogs("No user input was detected.", loggers);
                        break;
                    }
            }
        }

        private static void TestIfModesActive(List<IMode> modesList, string line)
        {
            foreach (IMode mode in modesList)
            {
                if (mode != null && mode.CheckLine(line))
                {
                    return;
                }
            }
        }

        static void Main(string[] args)
        {
            if (!IsProgramAlreadyRunning()) //if this is the only instance of the program
            {

                #region parameters
                bool isRunning = true, last_check_status = false, is_startup = true, isDownloading = false, isWindowVisible, isTesting;
                DateTime startOfProgram = DateTime.Now, logDate = DateTime.MinValue;
                IntPtr consoleHandle = ImportedFunctions.GetConsoleWindow();
                TimeSpan timeBetweenStartToClosing;
                Config config = new Config();
                string pathToFile, lastLineOfLog = "";
                StreamReader systemLogReader = null;
                StreamWriter logger = null, backupLogger = null;
                FileStream loggerStream = null, backupLoggerStream = null;
                List<StreamWriter> loggers = null;
                CSVWriter writer = null;
                #endregion

                #region config
                //read the parameters from the config file
                using (FileStream readConfigStream = new FileStream(Directory.GetCurrentDirectory() + "\\config.xml", FileMode.Open, FileAccess.Read))
                {
                    XmlSerializer reader = new XmlSerializer(typeof(Config));

                    config = (Config)reader.Deserialize(readConfigStream);

                }//using readConfigStream

                isTesting = config.TimeOutInMinutes == -1;
                #endregion

                #region Modes
                DownloadMode downloadMode = new DownloadMode(config.DownloadStartingLine, config.DownloadClosingLine, config.TimeUntilCheckDownload, config.DownloadCaption, config.TextboxDownloadMessage, MessageBoxButtons.OKCancel);
                RFTestingMode rfTestingMode = new RFTestingMode(config.RFTestingStartingLine, config.RFTestingClosingLine);
                RecordMode recordMode = new RecordMode(config.RecordStartingLine, config.RecordClosingLine, config.TimeUntilCheckRecord, config.RecordCaption, config.TextboxRecordMessage, MessageBoxButtons.OKCancel);
                ControlMode controlMode = new ControlMode(config.ControlStartingLine, config.ControlClosingLine);
                List<IMode> modesList = new List<IMode>
                {
                    recordMode,
                    rfTestingMode,
                    downloadMode,
                    controlMode
                };
                #endregion

                // Hide the console
                ImportedFunctions.ShowWindow(consoleHandle, ImportedFunctions.SW_HIDE);

                #region create directories
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\docs"))
                {
                    DirectoryInfo logDirectory = Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\docs");
                    logDirectory.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                    _ = Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\docs" + "\\logs");
                    _ = Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\docs" + "\\csv");
                    //_ = Directory.CreateDirectory("C:\\Users\\Users\\AppData\\logs"); //change to this before release
                    DirectoryInfo backupLogDirectory = Directory.CreateDirectory("C:\\Users\\Iftah\\AppData\\logs");
                    backupLogDirectory.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
                #endregion

                timeBetweenStartToClosing = TimeSpan.FromMinutes(config.TimeOutInMinutes);

                #region messagebox parameters
                MessageBoxController messageBox = new MessageBoxController(config.RFTestingCaption, //the caption
                                                    string.Format(config.RFTestingTextboxMessage, config.MessageBoxTimeoutInSecs), //the message
                                                    MessageBoxButtons.OKCancel, //the buttons
                                                    config.MessageBoxTimeoutInSecs); //the timeout of the messagebox
                #endregion

                while (isRunning)
                {
                    //if the date has changed then create another log file
                    if (DateTime.Now.Date > logDate.Date)
                    {
                        logDate = DateTime.Now.Date;
                        
                        if (loggerStream != null && logger != null)
                        {
                            //close the old streams
                            logger.Dispose();
                            loggerStream.Dispose();
                        }
                        if (backupLoggerStream != null && backupLogger != null)
                        {
                            //close the old streams
                            backupLogger.Dispose();
                            backupLoggerStream.Dispose();
                        }
                        string path = Directory.GetCurrentDirectory() + "\\docs" + "\\logs\\" + logDate.Date.ToString("dd_MM_yyyy") + ".txt";
                        loggerStream = new FileStream(path, FileMode.Append, FileAccess.Write);
                        logger = new StreamWriter(loggerStream)
                        {
                            AutoFlush = true
                        };
                        string pathToBackupLog = "C:\\Users\\Iftah\\AppData\\logs\\" + logDate.Date.ToString("dd_MM_yyyy") + ".txt"; //actual log path
                        //string pathToBackupLog = "C:\\Users\\Administrator\\AppData\\logs\\" + logDate.Date.ToString("dd_MM_yyyy") + ".txt";
                        backupLoggerStream = new FileStream(pathToBackupLog, FileMode.Append, FileAccess.Write);
                        backupLogger = new StreamWriter(backupLoggerStream)
                        {
                            AutoFlush = true
                        };
                        writer = new CSVWriter(Directory.GetCurrentDirectory() + "\\docs" + "\\csv\\" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".csv");
                        loggers = new List<StreamWriter> { logger, backupLogger };

                        ImportedFunctions.WriteToLogs("AutomaticDisconnect started a new log file.", loggers);
                        downloadMode.Logger = loggers;
                        recordMode.Logger = loggers;
                        controlMode.Logger = loggers;
                        rfTestingMode.Logger = loggers;
                    }
                    try
                    {
                        if (isTesting)
                        {
                            ImportedFunctions.WriteToLogs("Starded testing mode. closing AutomaticDisconnect.", loggers);
                            break;
                        }
                        Process relevantProcess = ImportedFunctions.GetRunningProcess(config.ProcessName);
                        if (relevantProcess != null)
                        {

                            if (is_startup) //if the was opened (wasn`t already running)
                            {
                                ImportedFunctions.WriteToLogs("The system was opened", loggers);
                                is_startup = false;
                                pathToFile = config.LogPath;
                                FileInfo logFile = ImportedFunctions.GetLatestFile(pathToFile);

                                fullfiledata = new FileStream(logFile.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                                systemLogReader = new StreamReader(fullfiledata);
                            }

                            string currentLastLine = ImportedFunctions.GetLastLineReadFromFile(systemLogReader);
                            if (!(lastLineOfLog == currentLastLine)) //only check if the last line was changed
                            {
                                lastLineOfLog = currentLastLine;
                                TestIfModesActive(modesList, lastLineOfLog);
                            }
                            isWindowVisible = false;

                            List<IntPtr> relevantWindowHandles = ImportedFunctions.GetWindowHandle(relevantProcess.Id, config.WindowName);

                            if (relevantWindowHandles.Count != 0)
                            {
                                isWindowVisible = ImportedFunctions.IsWindowsVisible(relevantWindowHandles);
                            }

                            if (isWindowVisible)
                            {

                                if (!last_check_status) //if RF testing just opened
                                {
                                    startOfProgram = DateTime.Now;
                                    last_check_status = true;
                                    ImportedFunctions.WriteToLogs("RF indicator was opened", loggers);
                                }
                                else if (downloadMode.IsInMode)
                                {
                                    if (!isDownloading)
                                    {
                                        ImportedFunctions.WriteToLogs("The system was in download mode. The system was not closed.", loggers);
                                        isDownloading = true;
                                    }
                                    Thread.Sleep(4000); //sleep for 4 seonds between reading the file and checking again
                                    continue;
                                }
                                else if (isDownloading && !downloadMode.IsInMode)
                                {
                                    TimeSpan lastDownloadTime = downloadMode.GetTimeline().Last().Value;
                                    ImportedFunctions.WriteToLogs(string.Format("download finished. is took {0} hours, {1} minutes and {2} seconds", lastDownloadTime.Hours,
                                        lastDownloadTime.Minutes, lastDownloadTime.Seconds), loggers);
                                    isDownloading = false;
                                }
                                else //if the program is in RF Testing
                                {
                                    if (DateTime.Now.Subtract(startOfProgram) >= timeBetweenStartToClosing)
                                    {
                                        if (config.MessageBoxTimeoutInSecs > 0)
                                        {
                                            messageBox.Show(logger);
                                        }
                                        if (messageBox.Result == DialogResult.Cancel)
                                        {
                                            ImportedFunctions.WriteToLogs("User clicked cancle on messagebox.", loggers);
                                            startOfProgram = DateTime.Now;
                                        }
                                        else // to close
                                        {
                                            WriteMessageboxInputToLog(messageBox.Result, loggers);

                                            ImportedFunctions.WriteToLogs("AutomaticDisconnect started trying closing the Indigma Rf testing by sending the " + config.ClosingKey + " key.", loggers);

                                            while (isWindowVisible)
                                            {
                                                ImportedFunctions.ChangeWindowViewState(relevantWindowHandles, ImportedFunctions.SW_SHOW);
                                                SendKeys.SendWait("{" + config.ClosingKey + "}"); //the key to close the program
                                                Thread.Sleep(100);
                                                isWindowVisible = ImportedFunctions.IsWindowsVisible(relevantWindowHandles);
                                            }
                                            ImportedFunctions.WriteToLogs("AutomaticDisconnect closed RF testing.", loggers);

                                        }
                                    }
                                    GC.Collect();
                                }//if the program is in RF testing
                                writer.Write(modesList);
                            }//if window is visible
                            else // If RF Testing not running
                            {
                                last_check_status = false;
                            }
                        }
                        else// Because the INDIGMA process is not running
                        {
                            last_check_status = false;
                            is_startup = true;
                        }
                        Thread.Sleep(500);
                    }
                    catch (Exception e)
                    {
                        ImportedFunctions.WriteToLogs("Execption: " + e.GetType(), loggers);
                        ImportedFunctions.WriteToLogs("\t Execption's Source: " + e.Source, loggers);
                        ImportedFunctions.WriteToLogs("\t Execption's StackTrace: " + e.StackTrace, loggers);
                        ImportedFunctions.WriteToLogs("\t Execption's Data: " + e.Data, loggers);
                        ImportedFunctions.WriteToLogs("\t Execption's Message: " + e.Message, loggers);
                        ImportedFunctions.WriteToLogs("\t Execption's InnerException: " + e.InnerException, loggers);
                    }
                }//while
                ImportedFunctions.WriteToLogs("closing program." + Environment.NewLine + "------------------------------------------------------------", loggers);
                logger.Dispose();
                loggerStream.Dispose();
            }//if not already running
        }//main


    }//class
}//namespace

