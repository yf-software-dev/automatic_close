﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IndigmaAutomaticDisconnect.Interfaces
{
    public class FullStringComparator : IStringComparator
    {
        private string lineToSearchFor;
        public FullStringComparator(string lineToSearchFor)
        {
            this.lineToSearchFor = lineToSearchFor;
        }
        public bool DoesLineMeesCondition(string line)
        {
            if (line != "")
            {
                return line.Contains(lineToSearchFor);
            }
            return false;
        }
    }
}
