﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace PrintProcesess
{
    class Program
    {

        delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn,
            IntPtr lParam);

        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);

            return handles;
        }


        private const uint WM_GETTEXT = 0x000D;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam,
            StringBuilder lParam);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        [STAThread]
        static void Main(string[] args)
        {
            IEnumerable<int> ids = Process.GetProcesses().Select(proc => proc.Id).Distinct();
            IEnumerable<IntPtr> processWindowHandles;
            Dictionary<IntPtr, bool> handleToBool = new Dictionary<IntPtr, bool>();

            foreach (int id in ids)
            {
                try
                {
                    processWindowHandles = EnumerateProcessWindowHandles(id);
                    Console.WriteLine("Process: " + Process.GetProcessById(id).ProcessName);
                    foreach (IntPtr handle in processWindowHandles)
                    {
                        if (handleToBool.ContainsKey(handle))
                            continue;
                        handleToBool[handle] = true;
                        StringBuilder message = new StringBuilder(1000);
                        _ = SendMessage(handle, WM_GETTEXT, message.Capacity, message);

                        if (!string.IsNullOrEmpty(message.ToString()))
                        {
                            Console.WriteLine("\t" + message + " - is visible: " + IsWindowVisible(handle) + " -window handle: " + handle);
                        }
                    }
                }
                catch { }
            }
            //Thread.Sleep(2000);

            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("press any key to quit");
            Console.WriteLine("-------------------------------------------------");
            _ = Console.ReadKey();
        }
        
    }
}
